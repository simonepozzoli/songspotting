FROM python:3.8.10

ENV PYTHONUNBUFFERED=1

# Install dependencies
RUN apt-get update -y && apt-get install -y libcurl4-openssl-dev libssl-dev libpq-dev libshout3-dev git netcat

# Copy application files
WORKDIR /srv/songspotting
ADD app .
COPY media /media


# Install requirements
COPY requirements.txt .
RUN pip install -r requirements.txt; rm requirements.txt

EXPOSE 80

ENTRYPOINT ["/srv/songspotting/entrypoint.sh"]

CMD ["./run.sh"]
