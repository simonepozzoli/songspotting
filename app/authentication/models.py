from django.db import models
import os

from django.conf import settings

import binascii

import authentication


class Token(models.Model):
    """
    An access token that is associated with a user.  This is essentially the same as the token model from Django REST Framework
    """
    key = models.CharField(max_length=40, primary_key=True)
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        related_name="token",
        on_delete=models.CASCADE
    )
    created = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = authentication.generate_token()
        return super(Token, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.key


class RegistrationRequest(models.Model):
    """
    A request to create a new user.
    """
    email = models.EmailField(unique=True)
    message = models.TextField(blank=True)
    token = models.CharField(max_length=40, blank=True, null=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = authentication.generate_token()
        return super(RegistrationRequest, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.email
