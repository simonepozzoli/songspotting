# Generated by Django 3.2 on 2021-08-12 19:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='RegistrationRequest',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('message', models.TextField(blank=True)),
                ('token', models.CharField(blank=True, max_length=40)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
