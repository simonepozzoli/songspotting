# -*- coding: utf-8 -*-
from django import forms

from authentication.models import RegistrationRequest


class RegistrationRequestForm(forms.ModelForm):
    class Meta:
        model = RegistrationRequest
        fields = ('email', 'message')
