from django.urls import path

import authentication.views as views

urlpatterns = [
    path('login/', views.auth_login),
    path('logout/', views.auth_logout),
    path('register/', views.auth_register),
    path('register/request-access/', views.request_registration),
    path('register/manage-request/', views.manage_registration_request),
    path('activate/', views.activate),
    path('invite/', views.invite)
]
