from authentication.models import Token

from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

User = get_user_model()


class TokenBackend(object):
    def authenticate(self, request, token=None):  # noqa
        """
        Try to find a user with the given token
        """
        try:
            t = Token.objects.get(key=token, user__is_active=True)
            return t.user
        except Token.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class UsernameOrEmailBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        if username is None or password is None:
            return
        user = self.get_user_by_email(username)
        if user:
            username = user.get_username()
        return super(UsernameOrEmailBackend, self).authenticate(
            request, username=username, password=password, **kwargs
        )

    @staticmethod
    def get_user_by_email(username):
        try:
            user = User.objects.get(email=username)
        except User.DoesNotExist:
            user = None
        return user
