# -*- coding: utf-8 -*-
import contextlib
import json
import smtplib
from django.conf import settings
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.password_validation import validate_password
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.http.response import (
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseRedirect
)
from django.shortcuts import render
from django.template.loader import render_to_string
from django.views.decorators.http import require_http_methods

import authentication
import authentication.forms

from authentication.models import RegistrationRequest

User = get_user_model()


@require_http_methods(['GET', 'POST'])
def auth_login(request):
    if request.method == 'GET':
        return _render_login_page(request)
    elif request.method == 'POST':
        return _login(request)


def auth_logout(request):
    logout(request)
    return HttpResponseRedirect(settings.HOMEPAGE)


def _render_login_page(request):
    _next = request.GET.get('next', settings.HOMEPAGE)
    return render(request, 'login_form.html', context={'next': _next})


def _login(request):
    try:
        credentials = json.loads(request.body)
    except json.JSONDecodeError:
        credentials = request.POST.dict()
    persistent_session = credentials.pop('_persistent', False)
    user = authenticate(
        request,
        username=credentials.get('username'),
        password=credentials.get('password')
    )
    if not user:
        return HttpResponse('Invalid credentials', status=401, reason='Unauthorized')
    login(request, user)
    if not persistent_session:
        request.session.set_expiry(0)
    return HttpResponse('Login successful')


@require_http_methods(['GET', 'POST'])
def request_registration(request):
    if request.method == 'GET':
        return render(
            request,
            'request_registration.html',
            context={'form': authentication.forms.RegistrationRequestForm()}
        )
    elif request.method == 'POST':
        form = authentication.forms.RegistrationRequestForm(request.POST)
        if form.is_valid():
            form_data = form.cleaned_data
            registration_request = RegistrationRequest(
                email=form_data['email'],
                message=form_data['message'],
                token=authentication.generate_token()
            )
            return _handle_registration_request(request, registration_request)
        else:
            return HttpResponseBadRequest(form.errors['email'][0])


def _handle_registration_request(request, registration):
    if User.objects.filter(email=registration.email):
        return HttpResponseBadRequest("A user with this email already exists")
    with contextlib.suppress(RegistrationRequest.DoesNotExist):
        RegistrationRequest.objects.get(email=registration.email)
        return HttpResponseBadRequest(
            "You already requested access to the site using this email. Please, wait "
            "for an approval by the administrators."
        )
    _send_registration_request_email(request, registration)
    registration.save()
    return HttpResponse(
        "Request successfully submitted. You will receive a feedback soon."
    )


def _send_registration_request_email(request, registration):
    manage_request_url = request.build_absolute_uri(
        location='/auth/register/manage-request/?t={}'.format(
            registration.token
        )
    )
    # Notify the user that request has been sent
    try:
        send_mail(
            subject="Your registration request",
            message=None,
            from_email=None,
            recipient_list=[registration.email],
            html_message=render_to_string(
                'confirm_registration_request_received_email.html',
                context={'manage_request_url': manage_request_url}
            )
        )
    except smtplib.SMTPRecipientsRefused:
        # Ignore malformed or non existing email addresses
        pass
    # Send registration request to admin
    manage_request_url += '&admin'
    send_mail(
        subject="New registration request",
        message=None,
        from_email=None,
        recipient_list=[settings.DEFAULT_FROM_EMAIL],
        html_message=render_to_string(
            'registration_request_email.html',
            context={
                'registration_request': registration,
                'manage_request_url': manage_request_url
            }
        )
    )


@require_http_methods(['GET', 'POST', 'DELETE'])
def manage_registration_request(request):
    try:
        registration_request = RegistrationRequest.objects.get(
            token=request.GET.get('t')
        )
    except RegistrationRequest.DoesNotExist:
        if request.method == 'GET':
            return render(request, '404.html', status=404)
        return HttpResponse(
            "Registration request not found, probably it has been already deleted.",
            status=404
        )
    if request.method == 'POST':
        if not request.user.is_staff:
            return HttpResponse(
                "You are not allowed to perform this operation.",
                status=401
            )
        _accept_registration_request(request, registration_request)
        return HttpResponse("Registration request accepted.")
    elif request.method == 'DELETE':
        _reject_registration_request(request, registration_request)
        return HttpResponse("Registration request rejected.")
    if 'admin' in request.GET:
        auth_wrapper = user_passes_test(lambda user: user.is_staff)
    else:
        auth_wrapper = user_passes_test(lambda user: True)
    get_response = auth_wrapper(render)
    return get_response(
        request,
        'manage_registration_request.html',
        context={'registration_request': registration_request}
    )


def _accept_registration_request(request, registration):
    invite_token = authentication.generate_token()
    register_url = request.build_absolute_uri(
        location='/auth/register/?t={}'.format(
            invite_token
        )
    )
    _send_invitation_link(registration.email, register_url)
    registration.delete()
    cache.set(invite_token, True)


def _reject_registration_request(request, registration):
    register_url = request.build_absolute_uri(
        location='/auth/register/request-access/'
    )
    send_mail(
        subject="Your registration request",
        message=None,
        from_email=None,
        recipient_list=[registration.email],
        html_message=render_to_string(
            'registration_request_rejected_email.html',
            context={'register_url': register_url}

        )
    )
    registration.delete()


@require_http_methods(['GET', 'POST'])
def auth_register(request):
    if request.method == 'GET':
        return _render_register_page(request)
    elif request.method == 'POST':
        return _register(request)


def _render_register_page(request):
    token = request.GET.get('t')
    if token is None:
        return HttpResponseRedirect('request-access/')
    if not _validate_token(token):
        return render(request, '401.html', status=401)
    return render(request, 'register_form.html', context={'token': token})


def _validate_token(token):
    return cache.get(token, False)


def _register(request):
    token = request.GET.get('t')
    if not _validate_token(token):
        return render(request, '401.html', status=401)
    try:
        user_data = json.loads(request.body)
    except json.JSONDecodeError:
        user_data = request.POST.dict()
    user = User(
        username=user_data['username'],
        email=user_data['email'],
        is_active=False
    )
    try:
        validate_password(user_data['password'], user=user)
    except ValidationError as e:
        return HttpResponseBadRequest(e.messages[0])
    user.set_password(user_data['password'])
    try:
        _validate_unique_user(user)
    except ValidationError as e:
        return HttpResponseBadRequest(str(e))
    account_activation_token = authentication.generate_token()
    activation_url = request.build_absolute_uri(
        location='/auth/activate/?t={}'.format(
            account_activation_token
        )
    )
    message = render_to_string(
        'activation_email.html',
        context={'activation_url': activation_url}
    )
    try:
        user.email_user(
            subject='Account activation',
            message='',
            html_message=message
        )
    except smtplib.SMTPRecipientsRefused:
        return HttpResponseBadRequest('Check your email')
    user.save()
    cache.set(account_activation_token, user.get_username())
    cache.delete(token)
    return HttpResponse('User {} created successfully'.format(user.get_username()))


def _validate_unique_user(user):
    try:
        user.validate_unique()
    except ValidationError as e:
        raise ValidationError('The username is already taken') from e
    if User.objects.filter(email=user.email).exists():
        raise ValidationError('The email is already taken')


@require_http_methods(['GET', 'POST'])
def activate(request):
    token = request.GET.get('t')
    username = cache.get(token)
    try:
        user = User.objects.get_by_natural_key(username)
    except User.DoesNotExist:
        return render(request, '404.html', status=404)
    user.is_active = True
    user.save()
    cache.delete(token)
    return render(
        request, 'account_activation.html'
    )


def _send_invitation_link(email, register_url):
    send_mail(
        subject='Invitation link',
        message=None,
        from_email=None,
        html_message=render_to_string(
            'invitation_email.html',
            context={'register_url': register_url}
        ),
        recipient_list=[email]
    )


def _invite_user(request):
    try:
        data = json.loads(request.body)
    except json.JSONDecodeError:
        data = request.POST.dict()
    invite_token = authentication.generate_token()
    register_url = request.build_absolute_uri(
        location='/auth/register/?t={}'.format(
            invite_token
        )
    )
    if 'email' in data:
        email = data.get('email')
        if not email:
            return HttpResponseBadRequest('Must provide a user email.')
        try:
            _send_invitation_link(email, register_url)
        except smtplib.SMTPRecipientsRefused:
            return HttpResponseBadRequest('Check the inserted email')
        cache.set(invite_token, True)
        return HttpResponse('An email has been sent to the provided address.')
    else:
        cache.set(invite_token, True)
        return HttpResponse(register_url)


@login_required
@require_http_methods(['GET', 'POST'])
def invite(request):
    if not request.user.is_staff:
        return render(request, '401.html', status=401)
    if request.method == 'GET':
        return render(request, 'invite_users.html')
    elif request.method == 'POST':
        return _invite_user(request)
