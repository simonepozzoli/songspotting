from django.conf import settings
from django.db import models

from channels_presence.models import Room as ChannelsPresenceRoom


class Room(ChannelsPresenceRoom):
    class Meta:
        proxy = True

    def new_message(self, user, text):
        """
        Save new message in chat room
        """
        return Message.objects.create(
            user=user,
            text=text,
            room=self
        )


class RoomManager(models.Manager):
    @staticmethod
    def by_room(room):
        qs = Message.objects.filter(room=room).order_by('-timestamp')
        return qs


class Message(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
        related_name='chat_messages'
    )
    room = models.ForeignKey('Room', on_delete=models.CASCADE, related_name='messages')
    timestamp = models.DateTimeField(auto_now_add=True)
    text = models.TextField(blank=False)

    objects = RoomManager()

    def __str__(self):
        return self.text
