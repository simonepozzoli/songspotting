from django.conf.urls import url

from chat import consumers

websocket_urlpatterns = [
    url(r'(?P<room_name>\w+)/$', consumers.ChatConsumer.as_asgi()),
]
