from django.contrib import admin
from django.core.paginator import Paginator
from django.core.cache import cache

import chat.models


admin.site.register(chat.models.Room)


# Resource: http://masnun.rocks/2017/03/20/django-admin-expensive-count-all-queries/
class CachingPaginator(Paginator):
    def _get_count(self):

        if not hasattr(self, "_count"):
            self._count = None

        if self._count is None:
            try:
                key = "adm:{0}:count".format(hash(self.object_list.query.__str__()))
                self._count = cache.get(key, -1)
                if self._count == -1:
                    self._count = super().count
                    cache.set(key, self._count, 3600)

            except Exception:
                self._count = len(self.object_list)
        return self._count

    count = property(_get_count)


class MessageAdmin(admin.ModelAdmin):
    list_filter = ['room', 'user', 'timestamp']
    list_display = ['room', 'user', 'timestamp', 'text']
    search_fields = ['room__name', 'user__username', 'text']
    readonly_fields = ['id', 'user', 'room', 'timestamp']
    ordering = ['room', '-timestamp']
    show_full_result_count = False
    paginator = CachingPaginator


admin.site.register(chat.models.Message, MessageAdmin)
