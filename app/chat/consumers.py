import json

from asgiref.sync import async_to_sync
from channels.exceptions import DenyConnection
from channels.generic.websocket import WebsocketConsumer
from channels_presence.decorators import touch_presence
from channels_presence.models import Presence

from django.utils import timezone

from chat.models import Room


class ChatConsumer(WebsocketConsumer):
    @property
    def room_name(self):
        return self.scope['url_route']['kwargs']['room_name']

    def _join_room(self):
        self.room, created = Room.objects.get_or_create(channel_name=self.room_name)
        self.room.add_presence(self.channel_name, self.user)
        # If user just joined the room, notify other connected users
        if Presence.objects.filter(room=self.room, user=self.user).count() == 1:
            self._send_to_group(
                {
                    'type': 'user_connected',
                    'user': self.user.get_username(),
                    'timestamp': timezone.now().isoformat()
                }
            )

    def _leave_room(self):
        self.room.remove_presence(self.channel_name)
        # If user has no other connections open, notify other connected users
        if self.user not in self.room.get_users():
            self._send_to_group(
                {
                    'type': 'user_disconnected',
                    'user': self.user.get_username(),
                    'timestamp': timezone.now().isoformat()
                }
            )

    def _store_message(self, text):
        return self.room.new_message(
            self.user,
            text
        )

    def _send_to_group(self, message):
        async_to_sync(self.channel_layer.group_send)(
            self.room_name,
            message
        )

    def connect(self):
        self.user = self.scope['user']
        if not self.user.is_authenticated:
            raise DenyConnection()
        super(ChatConsumer, self).connect()
        self._join_room()

    def disconnect(self, close_code):
        if hasattr(self, 'room'):
            self._leave_room()

    @touch_presence
    def receive(self, text_data=None, bytes_data=None):
        """
        Receive message from a user and forward it to all
        connected users
        """
        data = json.loads(text_data)
        message_type = data['type']
        if message_type == 'chat_message':
            message_text = data['text']

            message = self._store_message(message_text)

            # Forward message to all connected clients
            self._send_to_group(
                {
                    'type': 'chat_message',
                    'text': message.text,
                    'user': message.user.get_username(),
                    'timestamp': message.timestamp.isoformat()
                }
            )

    def chat_message(self, event):
        # Send message to WebSocket
        self.send(text_data=json.dumps(event))

    def user_connected(self, event):
        if self.user.get_username() != event['user']:
            # Send message to WebSocket
            self.send(text_data=json.dumps(event))

    def user_disconnected(self, event):
        if self.user.get_username() != event['user']:
            # Send message to WebSocket
            self.send(text_data=json.dumps(event))
