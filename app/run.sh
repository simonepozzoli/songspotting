#!/bin/bash

celery -A songspotting beat --detach
echo "Started Celery Beat"

celery -A songspotting worker --detach
echo "Started Celery Worker"

daphne -b 0.0.0.0 -p 80 songspotting.asgi:application