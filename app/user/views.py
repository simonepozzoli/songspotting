# -*- coding: utf-8 -*-
import json
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.password_validation import validate_password
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.http.response import (
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseRedirect
)
from django.shortcuts import render
from django.template.loader import render_to_string
from django.views.decorators.http import require_http_methods

import authentication

User = get_user_model()


@require_http_methods(['POST'])
def send_password_reset_email(request):
    try:
        username = json.loads(request.body).get('username')
    except json.JSONDecodeError:
        username = request.POST.get('username')
    try:
        user = User.objects.get_by_natural_key(username)
    except User.DoesNotExist:
        pass
    else:
        token = authentication.generate_token()
        password_reset_url = request.build_absolute_uri(
            location='/user/reset-password/?t={}'.format(token)
        )
        message = render_to_string(
            'reset_password_email.html',
            context={'password_reset_url': password_reset_url}
        )
        user.email_user(
            subject='Reset your password',
            message='',
            html_message=message,
            fail_silently=True
        )
        cache.set(token, user.get_username())
    return HttpResponse(
        'If a user with this username exists, a reset link will be sent to her/his '
        'email address.'
    )


def _reset_user_password(user, password):
    """
    Sets a user password to the one provided.

    Arguments:
        user (django.contrib.auth.models.User): The User object.
        password (str): The new password.
    """
    validate_password(password, user)
    user.set_password(password)


@require_http_methods(['GET', 'POST'])
def reset_password(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            # direct access to the password change form
            return render(request, 'reset_password_form.html')
        token = request.GET.get('t')
        try:
            User.objects.get_by_natural_key(cache.get(token))
        except User.DoesNotExist:
            # return page to request password change
            return render(request, 'reset_password.html')
        else:
            # the user provided a token, probably received with a password reset email,
            # give access to the password change form
            return render(request, 'reset_password_form.html', context={'token': token})
    elif request.method == 'POST':
        data = request.POST.dict()
        if request.user.is_authenticated:
            user = request.user
        else:
            try:
                user = User.objects.get_by_natural_key(cache.get(data.get('token')))
            except User.DoesNotExist:
                # return page to request password change
                return HttpResponse('Unauthorized', status=401)
        try:
            validate_password(data.get('password'), user)
        except ValidationError as e:
            return HttpResponseBadRequest(e.messages[0])
        user.set_password(data.get('password'))
        user.save()
        cache.delete(data.get('token'))
        return HttpResponse('Password correctly reset.')


@login_required
@require_http_methods(['POST'])
def delete_user(request):
    """
    Deletes a user account.
    """
    request.user.delete()
    return HttpResponse('User {} has been deleted'.format(request.user.get_username()))
