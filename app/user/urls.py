from django.urls import path

import user.views as views

urlpatterns = [
    path('_delete/', views.delete_user),
    path('reset-password/', views.reset_password),
    path('reset-password/send-email/', views.send_password_reset_email),
]
