import json
import logging

from asgiref.sync import async_to_sync
from channels.exceptions import DenyConnection
from channels.generic.websocket import WebsocketConsumer
from channels.layers import get_channel_layer
from django.core.files import File

import discogs_client.exceptions

import radio.models
import radio.tasks
from radio.tools import discogs

channel_layer = get_channel_layer()

RADIO_GROUP = 'radio-channel-group'

logger = logging.getLogger(__name__)


class StreamMetadataConsumer(WebsocketConsumer):

    groups = [RADIO_GROUP]

    def _send_to_group(self, message):
        async_to_sync(self.channel_layer.group_send)(
            RADIO_GROUP,
            message
        )

    def _send_error(self, message, stream_id):
        self.send(text_data=json.dumps(
            {
                'type': 'error',
                'text': message,
                'stream_id': stream_id
            }
        ))

    def connect(self):
        if not self.scope['user'].is_authenticated:
            raise DenyConnection()
        super(StreamMetadataConsumer, self).connect()

    def receive(self, text_data=None, bytes_data=None):
        """
        Retrieve release metadata from Discogs and
        set it to the stream
        """
        data = json.loads(text_data)
        message_type = data['type']
        if message_type == 'set_metadata':
            release_id = data['release_id']
            stream_id = data['stream_id']

            try:
                metadata = discogs.set_stream_metadata(stream_id, release_id)
            except discogs_client.exceptions.HTTPError as err:
                message = err.msg
                self._send_error(message, stream_id)
            except radio.models.Stream.DoesNotExist:
                message = "Stream with ID [{}] does not exist".format(stream_id)
                self._send_error(message, stream_id)
            else:
                # Forward message to all connected clients
                message = data.copy()
                message['metadata'] = metadata
                self._send_to_group(message)
        if message_type == 'delete_metadata':
            stream_id = data['stream_id']
            try:
                metadata = discogs.delete_stream_metadata(stream_id)
            except radio.models.Stream.DoesNotExist:
                message = "Stream with ID [{}] does not exist".format(stream_id)
                self._send_error(message, stream_id)
            else:
                message = data.copy()
                message['type'] = 'set_metadata'
                message['metadata'] = metadata
                self._send_to_group(message)

    def set_metadata(self, event):
        """
        Update stream metadata
        """
        self.send(text_data=json.dumps(event))

    def add_stream(self, event):
        """
        Show new stream in radio homepage
        """
        self.send(text_data=json.dumps(event))

    def remove_stream(self, event):
        """
        Remove deactivated stream from radio homepage
        """
        self.send(text_data=json.dumps(event))


class IcecastStreamConsumer(WebsocketConsumer):
    """
    Receive audio stream from user and upload it to Icecast
    """
    clients = {}

    def __init__(self, *args, **kwargs):
        super(IcecastStreamConsumer, self).__init__(*args, **kwargs)
        self.client = None
        self.stream = None
        self.recording = None
        self.recording_file = None
        self.recording_enabled = False

    def connect(self):
        # user must be authenticated and be the owner of the stream
        if not self.scope['user'].is_authenticated:
            raise DenyConnection()
        try:
            self.stream = radio.models.Stream.objects.select_related(
                'owner'
            ).get(
                id=self.scope['url_route']['kwargs']['stream_id']
            )
        except radio.models.Stream.DoesNotExist:
            raise DenyConnection()
        if not self.scope['user'].is_authenticated or self.stream.owner.pk != self.scope['user'].pk:
            raise DenyConnection()

        # create Icecast connection
        self.client = self.stream.get_mp3_client()
        self.client.open()

        self.stream.activate()
        self.recording = radio.models.Recording.init_from_stream(self.stream)
        super(IcecastStreamConsumer, self).connect()

    def disconnect(self, code):
        if self.recording_file:
            self.recording_file.close()
            with self.recording.get_read_file() as f:
                self.recording.file.save(self.recording.file_name, File(f))
        super().disconnect(code)

    def receive(self, text_data=None, bytes_data=None):
        """
        Receive MP3 sample from user and send it to Icecast stream
        """
        if bytes_data:
            self.client.send(bytes_data)
            if self.recording_enabled:
                self.write_recording(bytes_data)
        if text_data:
            data = json.loads(text_data)
            self.recording_enabled = data['recording_enabled']

    def write_recording(self, bytes_data):
        if not self.recording_file:
            self.recording_file = self.recording.get_write_file()
        self.recording_file.write(bytes_data)
