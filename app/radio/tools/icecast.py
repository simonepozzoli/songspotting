import requests
from django.conf import settings

ICECAST_STATS_PATH = '/status-json.xsl'
ICECAST_STATS_URL = f'http://{settings.ICECAST_HOST}:{settings.ICECAST_PORT}{ICECAST_STATS_PATH}'


def get_sources_info():
    response = requests.get(ICECAST_STATS_URL)
    response.raise_for_status()
    sources = response.json()['icestats'].get('source', [])
    if isinstance(sources, dict):
        sources = [sources]
    return sources
