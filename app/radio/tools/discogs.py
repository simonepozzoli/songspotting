# -*- coding: utf-8 -*-
from django.conf import settings

from discogs_client import Client

import radio.models


client = Client("SongSpotting/1.0", user_token=settings.DISCOGS_USER_TOKEN)


def set_stream_metadata(stream_id, release_id):
    release = client.release(release_id)
    release.refresh()
    stream = radio.models.Stream.objects.get(id=stream_id)
    metadata = release.data
    stream.metadata = metadata
    return metadata


def get_stream_metadata(stream_id):
    stream = radio.models.Stream.objects.get(id=stream_id)
    return stream.metadata


def delete_stream_metadata(stream_id):
    stream = radio.models.Stream.objects.get(id=stream_id)
    stream.metadata = None
    return None
