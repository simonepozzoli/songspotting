# -*- coding: utf-8 -*-
from django.urls import path

import radio.views as views

urlpatterns = [
    path('can-listen/', views.can_listen),
    path('playlist/<str:filetype>/<int:stream_id>/', views.icecast_playlist),
    path('notifications/', views.manage_notifications, name='manage-notifications'),
    path('discogs/metadata/<int:stream_id>/', views.stream_metadata_from_discogs),
    path('recordings/', views.manage_recordings),
    path('recordings/<int:recording_id>/download/', views.download_recording, name='download-recording'),
    path('recordings/<int:recording_id>/delete/', views.delete_recording, name='delete-recording'),
    path('streams/', views.manage_streams),
    path('stream/<int:stream_id>/', views.manage_stream),
    path(
        'stream/<int:stream_id>/listeners-count/',
        views.get_stream_listeners_count,
        name='get-stream-listeners-count',
    ),
    path('', views.radio_home),
]
