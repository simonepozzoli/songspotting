# -*- coding: utf-8 -*-
from django import forms
from django.db.utils import OperationalError

from radio.models import Stream


class StreamMultipleChoiceField(forms.MultipleChoiceField):
    def __init__(self, *, choices=(), **kwargs):
        super().__init__(**kwargs)
        self.choices = choices

    @staticmethod
    def _get_all_streams_choices():
        try:
            streams = list(Stream.objects.all().order_by('owner__username', 'name').values_list('mountpoint', 'name'))
        except OperationalError:
            streams = ()
        return streams


class StreamMultipleChoiceForm(forms.Form):
    Streams = StreamMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple(),
    )

    def __init__(self, *args, **kwargs):
        self.declared_fields["Streams"].choices = Stream.objects.all().order_by(
            'owner__username', 'name'
        ).values_list(
            'mountpoint', 'name'
        )
        super(StreamMultipleChoiceForm, self).__init__(*args, **kwargs)
