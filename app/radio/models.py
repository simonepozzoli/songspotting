import json
import os
import urllib.parse

import shout
from random_username.generate import generate_username

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.utils import timezone

import radio.tasks
from radio.tools import icecast

_User = get_user_model()


class Stream(models.Model):
    mountpoint = models.TextField(unique=True)
    active = models.BooleanField(default=False)
    description = models.TextField(null=True, blank=True)
    name = models.TextField(unique=True)
    url = models.TextField(null=True, blank=True)
    subscribers = models.ManyToManyField(_User, blank=True)
    start_datetime = models.DateTimeField(null=True, blank=True)
    owner = models.ForeignKey(_User, on_delete=models.SET_NULL, null=True, blank=True, related_name='own_streams')
    background = models.ImageField(
        upload_to='radio/stream_background',
        default='radio/stream_background/default.jpg'
    )

    @classmethod
    def list_backgrounds(cls):
        background_field = cls.background.field
        try:
            return background_field.storage.listdir(
                background_field.upload_to
            )
        except FileNotFoundError:
            return [], []

    def get_mp3_client(self, bitrate='320', samplerate='44100', channels='2'):
        mp3_client = shout.Shout()
        mp3_client.host = settings.ICECAST_HOST
        mp3_client.port = settings.ICECAST_PORT
        mp3_client.password = settings.ICECAST_SOURCE_PASSWORD
        mp3_client.format = 'mp3'
        mp3_client.audio_info = {
            shout.SHOUT_AI_BITRATE: bitrate,
            shout.SHOUT_AI_SAMPLERATE: samplerate,
            shout.SHOUT_AI_CHANNELS: channels
        }
        mp3_client.mount = self.mountpoint
        mp3_client.name = self.name
        return mp3_client

    def to_dict(self):
        return {
            'id': self.id,
            'mountpoint': self.mountpoint,
            'active': self.active,
            'description': self.description,
            'name': self.name,
            'url': self.url,
            'start_datetime': self.start_datetime.isoformat() if self.start_datetime else None,
            'owner': self.owner.get_username() if self.owner else None,
            'metadata': self.metadata,
            'background': {
                'url': self.background.url
            },
            "listeners_count": self.get_listeners_count(),
        }

    def to_json(self):
        return json.dumps(self.to_dict())

    def save(self, *args, **kwargs):
        self.mountpoint = self.mountpoint or '/' + generate_username()[0]
        self.url = '/icecast' + self.mountpoint
        super(Stream, self).save(*args, **kwargs)

    @property
    def metadata_cache_key(self):
        return f"audio_stream_metadata_{self.id}"

    @property
    def metadata(self):
        return cache.get(self.metadata_cache_key)

    @metadata.setter
    def metadata(self, metadata):
        if metadata is None:
            cache.delete(self.metadata_cache_key)
        else:
            cache.set(self.metadata_cache_key, metadata)

    def activate(self, send_notification=True):
        if self.active:
            return
        self.start_datetime = timezone.now()
        self.active = True
        self.save()
        if send_notification:
            radio.tasks.send_notification_for_active_stream.apply_async(
                args=(self.id,),
                expires=10
            )
        radio.tasks.add_stream_to_radio_homepage.apply_async(args=(self.id,), expires=10)

    def deactivate(self):
        self.start_datetime = None
        self.active = False
        self.metadata = None
        self.save()

    def get_recording_filename(self):
        return RecordingStorage().get_valid_name(f'{self.name}_{self.start_datetime}.mp3')

    def get_icecast_source_info(self):
        for source in icecast.get_sources_info():
            if urllib.parse.urlparse(source['listenurl']).path == self.mountpoint:
                return source
        return None

    def get_listeners_count(self):
        icecast_source = self.get_icecast_source_info()
        return icecast_source["listeners"] if icecast_source is not None else 0

    def __str__(self):
        return self.name


class RecordingStorage(FileSystemStorage):
    def get_available_name(self, name, max_length=None):
        if self.exists(name):
            self.delete(name)
        return super().get_available_name(name, max_length)


class Recording(models.Model):
    file = models.FileField(upload_to='radio/recordings', storage=RecordingStorage())
    stream = models.ForeignKey(Stream, on_delete=models.CASCADE, related_name='recordings')
    start_datetime = models.DateTimeField(auto_now_add=True)
    description = models.TextField(null=True, blank=True)

    @classmethod
    def init_from_stream(cls, stream: "Stream") -> "Recording":
        return cls(
            stream=stream,
            description=stream.description,
            start_datetime=timezone.now()
        )

    def get_write_file(self):
        return open(self.get_file_path(), 'wb')

    def get_read_file(self):
        return open(self.get_file_path(), 'rb')

    @property
    def file_name(self):
        if self.file:
            return self.file.name.split('/')[-1]
        return self.file.storage.get_valid_name(f'{self.stream.name}_{self.start_datetime}.mp3')

    def get_file_path(self):
        return os.path.join(settings.MEDIA_ROOT, 'radio', 'recordings', self.file_name)

    def __str__(self):
        return f'{self.stream} - {self.start_datetime}'
