# -*- coding: utf-8 -*-
import logging
import smtplib
import urllib.parse

from asgiref.sync import async_to_sync
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.template.loader import render_to_string
from django.urls import reverse

from channels.layers import DEFAULT_CHANNEL_LAYER, get_channel_layer

import radio.consumers
import radio.models as models
from radio.tools import icecast

from songspotting.celery import app


logger = logging.getLogger(__name__)

_User = get_user_model()


@app.task
def add_stream_to_radio_homepage(stream_id):
    """
    Update the radio home page showing the new stream
    """
    stream = radio.models.Stream.objects.get(id=stream_id)
    channel_layer = get_channel_layer(DEFAULT_CHANNEL_LAYER)
    message = {
        'type': 'add_stream',
        'stream': stream.to_dict()
    }
    # send message
    async_to_sync(channel_layer.group_send)(
        radio.consumers.RADIO_GROUP,
        message
    )
    logger.info(f'Added stream [{stream.name}] to radio homepage')


@app.task
def remove_stream_from_radio_homepage(stream_id):
    """
    Update the radio home page removing the deactivated stream
    """
    stream = radio.models.Stream.objects.get(id=stream_id)
    channel_layer = get_channel_layer(DEFAULT_CHANNEL_LAYER)
    message = {
        'type': 'remove_stream',
        'stream': stream.to_dict(),
    }
    # send message
    async_to_sync(channel_layer.group_send)(
        radio.consumers.RADIO_GROUP,
        message
    )
    logger.info(f'Removed stream [{stream.name}] from radio homepage')


@app.task
def send_notification_for_active_stream(stream_id):
    stream = radio.models.Stream.objects.get(id=stream_id)
    current_site = Site.objects.get_current()
    listen_url = 'http://{}{}'.format(current_site.domain, settings.HOMEPAGE)
    message = render_to_string(
        'activate_stream_email.html',
        context={
            'listen_url': listen_url,
            'notification_settings_url': f'http://{current_site.domain}{reverse("manage-notifications")}',
            'stream': stream
        }
    )
    for user in stream.subscribers.filter(is_active=True, email__isnull=False):
        logger.info(
            'Notifying user [{}] that stream [{}] is online'.format(
                user.username, stream.name
            )
        )
        try:
            user.email_user(
                subject=f'{stream.name} is back!',
                message='',
                html_message=message
            )
        except smtplib.SMTPRecipientsRefused as e:
            logger.error("Error sending mail to [{}]: {}".format(user, e))


@app.task
def send_notification_for_new_stream(stream_id):
    stream = radio.models.Stream.objects.get(id=stream_id)
    current_site = Site.objects.get_current()
    listen_url = 'http://{}{}'.format(current_site.domain, settings.HOMEPAGE)
    message = render_to_string(
        'new_stream_email.html',
        context={
            'listen_url': listen_url,
            'stream': stream
        }
    )
    for user in _User.objects.filter(is_active=True, email__isnull=False):
        logger.info(
            'Notifying user [{}] that new stream [{}] is available'.format(
                user.username, stream.name
            )
        )
        try:
            user.email_user(
                subject='New stream available on SongSpotting',
                message='',
                html_message=message
            )
        except smtplib.SMTPRecipientsRefused as e:
            logger.error("Error sending mail to [{}]: {}".format(user, e))


@app.task
def sync_streams_state_from_icecast():
    sources = icecast.get_sources_info()
    stream_mountpoints = [
        urllib.parse.urlparse(source['listenurl']).path
        for source in sources
    ]
    streams_to_be_activated = radio.models.Stream.objects.filter(
        mountpoint__in=stream_mountpoints,
        active=False
    )
    streams_to_be_deactivated = radio.models.Stream.objects.filter(
        active=True
    ).exclude(
        mountpoint__in=stream_mountpoints,
    )
    for stream in streams_to_be_activated:
        logger.info('Activating stream [{}]'.format(stream.name))
        stream.activate(send_notification=False)
    for stream in streams_to_be_deactivated:
        logger.info('Deactivating stream [{}]'.format(stream.name))
        stream.deactivate()
        remove_stream_from_radio_homepage.apply_async(args=(stream.id,), expires=10)
