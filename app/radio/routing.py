from django.conf.urls import url

from radio import consumers

websocket_urlpatterns = [
    url(r'metadata/$', consumers.StreamMetadataConsumer.as_asgi()),
    url(r'stream/(?P<stream_id>[0-9]+)/$', consumers.IcecastStreamConsumer.as_asgi()),
]
