# -*- coding: utf-8 -*-
import json
import urllib.parse
from xml.etree import ElementTree

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.db.utils import IntegrityError
from django.http import HttpResponse, QueryDict
from django.shortcuts import get_object_or_404, render
from django.views.decorators.http import require_http_methods

import discogs_client.exceptions
from xspf_lib import Playlist

import authentication

import radio.forms
import radio.models
import radio.proxy
import radio.tasks  # this ensures tasks are loaded

from radio.tools import discogs


User = get_user_model()


def can_listen(request):
    """
    Check whether user can listen to streams.
    If user is not authenticated,
    try to authenticate it with a listen token.
    """
    user = request.user
    if not request.user.is_authenticated:
        listen_token = request.GET.get('t')
        if listen_token:
            token_username = cache.get(listen_token)
            if token_username:
                try:
                    token_user = User.objects.get_by_natural_key(token_username)
                except User.DoesNotExist:
                    pass
                else:
                    if token_user.is_active:
                        user = token_user
    if user.is_authenticated:
        return HttpResponse(
            f'User {request.user.get_username()} can listen to streams',
            status=200
        )
    return HttpResponse('Not authenticated', status=401)


@login_required
@require_http_methods(['GET'])
def icecast_playlist(request, filetype, stream_id):
    """
    Parse and return playlist files created by Icecast server
    """
    stream = get_object_or_404(radio.models.Stream, id=stream_id)
    playlist_url = urllib.parse.urljoin(settings.ICECAST_URL, f'{stream.mountpoint}.{filetype}')
    icecast_response = radio.proxy.get_response(request, playlist_url)
    if icecast_response.status_code == 200:
        listen_token = authentication.generate_token()
        listen_url = urllib.parse.urljoin(request.build_absolute_uri(), stream.url) + '?t={}'.format(listen_token)
        if filetype == 'm3u':
            content = listen_url + '\r\n'
        elif filetype == 'xspf':
            content = _parse_xspf(icecast_response.text, listen_url)
        else:
            return HttpResponse(f'Unsupported playlist filetype "{filetype}"', status=400)
        icecast_response.headers['Content-Length'] = len(content)
        filename = f'{stream.mountpoint[1:]}.{filetype}'
        icecast_response.headers['Content-Disposition'] = f'attachment; filename={filename}'
        icecast_response.headers.pop('Connection', '')
        cache.set(
            listen_token,
            request.user.get_username(),
            timeout=settings.LISTEN_TOKEN_AGE
        )
    else:
        content = icecast_response.content
    response = HttpResponse(
        content=content,
        headers=icecast_response.headers,
        status=icecast_response.status_code
    )
    return response


def _parse_xspf(xspf_string, location):
    """
    Parse a XSPF playlist file changing the location URL
    """
    xml_data = ElementTree.fromstring(xspf_string)
    playlist = Playlist.parse_from_xml_element(xml_data)
    playlist.trackList[0].location = [location]
    return playlist.xml_string()


@login_required
@require_http_methods(['GET'])
def radio_home(request):
    """
    Render radio homepage
    """
    streams = radio.models.Stream.objects.filter(active=True)
    return render(
        request,
        'radio_home.html',
        context={'streams': streams}
    )


@login_required
@require_http_methods(['GET', 'POST'])
def manage_notifications(request):
    """
    Manage stream notifications user preferences
    """
    if request.method == 'POST':
        streams_form = radio.forms.StreamMultipleChoiceForm(request.POST)
        if streams_form.is_valid():
            mountpoints = streams_form.cleaned_data.get('Streams')
        else:
            mountpoints = []
        streams_to_subscribe = radio.models.Stream.objects.filter(
            mountpoint__in=mountpoints
        ).exclude(
            subscribers=request.user
        )
        for s in streams_to_subscribe:
            s.subscribers.add(request.user)
            s.save()
        streams_to_unsubscribe = radio.models.Stream.objects.exclude(
            mountpoint__in=mountpoints
        ).filter(
            subscribers=request.user
        )
        for s in streams_to_unsubscribe:
            s.subscribers.remove(request.user)
            s.save()
        return HttpResponse("Preferences updated")
    return render(
        request,
        'radio_notifications.html',
        context={
            'streams': radio.models.Stream.objects.all().order_by('owner__username', 'name')
        }
    )


@login_required
@require_http_methods(['GET', 'PUT', 'DELETE'])
def stream_metadata_from_discogs(request, stream_id):
    """
    Update stream metadata with information taken from Discogs release
    """
    if request.method == 'PUT':
        release_id = QueryDict(request.body).get('release_id')
        try:
            metadata = discogs.set_stream_metadata(stream_id, release_id)
        except discogs_client.exceptions.HTTPError as err:
            return HttpResponse(
                err.msg,
                content_type="text/html",
                status=404
            )
        except radio.models.Stream.DoesNotExist:
            return HttpResponse(
                "Stream with ID [{}] does not exist".format(stream_id),
                content_type="text/html",
                status=404
            )
    elif request.method == 'DELETE':
        metadata = discogs.delete_stream_metadata(stream_id)
    else:
        try:
            metadata = discogs.get_stream_metadata(stream_id)
        except radio.models.Stream.DoesNotExist:
            return HttpResponse(
                "Stream with ID [{}] does not exist".format(stream_id),
                content_type="text/html",
                status=404
            )
    return HttpResponse(
        json.dumps({
            "metadata": metadata
        }),
        content_type="application/json"
    )


@login_required
@require_http_methods(['GET', 'POST'])
def manage_streams(request):
    if request.method == 'POST':
        stream_data = json.loads(request.body)
        try:
            stream = radio.models.Stream.objects.create(owner=request.user, **stream_data)
        except IntegrityError:
            return HttpResponse(
                'A stream with this name already exists.',
                status=400
            )
        return HttpResponse(
            stream.to_json(), status=200, content_type='application/json'
        )
    return render(request, 'manage_streams.html', {
        'streams': radio.models.Stream.objects.filter(owner=request.user),
    })


@login_required
@require_http_methods(['GET', 'PUT', 'DELETE'])
def manage_stream(request, stream_id):
    try:
        stream = radio.models.Stream.objects.get(id=stream_id)
    except radio.models.Stream.DoesNotExist:
        return HttpResponse(
            'Stream not found',
            status=404
        )
    if request.method == 'DELETE':
        stream.deactivate()
        stream.delete()
        return HttpResponse(
            'Stream [] deleted'.format(stream_id),
            status=200
        )
    if request.method == 'PUT':
        stream_data = json.loads(request.body)
        for key, value in stream_data.items():
            setattr(stream_data, key, value)
        stream.save()
        return HttpResponse(
            stream.to_json(), status=200, content_type='application/json'
        )
    return render(request, 'manage_stream.html', {
        'stream': stream,
    })


@login_required
@require_http_methods(["GET"])
def manage_recordings(request):
    streams = radio.models.Stream.objects.exclude(recordings=None)
    return render(
        request,
        'list_recordings.html',
        {
            'stream_recordings': {
                stream: stream.recordings.all().order_by('-start_datetime')
                for stream in streams
            }
        }
    )


@login_required
@require_http_methods(["GET"])
def download_recording(request, recording_id):
    recording = get_object_or_404(radio.models.Recording, id=recording_id)
    response = HttpResponse(recording.file, content_type='audio/mpeg')
    response['Content-Disposition'] = f'attachment; filename={recording.file_name}'
    return response


@login_required
@require_http_methods(["DELETE"])
def delete_recording(request, recording_id):
    recording = get_object_or_404(radio.models.Recording, id=recording_id)
    if recording.stream.owner.pk != request.user.pk:
        return HttpResponse("Can't delete recording", status=401)
    recording.delete()
    return HttpResponse("Recording deleted")


@login_required
@require_http_methods(["GET"])
def get_stream_listeners_count(request, stream_id):
    stream = get_object_or_404(radio.models.Stream, id=stream_id)
    return HttpResponse(stream.get_listeners_count())
