from django.contrib import admin

import radio.models as models

# Register your models here.


class StreamAdmin(admin.ModelAdmin):
    readonly_fields = ('active', 'mountpoint', 'start_datetime', 'url')
    list_display = ('name', 'mountpoint', 'owner', 'active')
    list_display_links = ('name',)


admin.site.register(models.Stream, StreamAdmin)


class RecordingAdmin(admin.ModelAdmin):
    readonly_fields = ('file', 'stream', 'start_datetime')


admin.site.register(models.Recording, RecordingAdmin)
