# Generated by Django 3.2 on 2021-07-19 20:30

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('radio', '0002_alter_stream_start_datetime'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stream',
            name='subscribers',
            field=models.ManyToManyField(blank=True, null=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
