from django.db import migrations


def create_periodic_tasks(apps, schema_editor):
    """Create radio notifications periodic task"""
    IntervalSchedule = apps.get_model('django_celery_beat', 'IntervalSchedule')  # noqa
    interval, _ = IntervalSchedule.objects.get_or_create(every=5, period='seconds')
    PeriodicTask = apps.get_model('django_celery_beat', 'PeriodicTask')  # noqa
    PeriodicTask.objects.get_or_create(
        name='Streams notifications',
        interval=interval,
        task='radio.tasks.streams_notifications',
        description='Send radio streams notifications to users in the following case:\n\n'
                    ' - A new stream is available;\n'
                    ' - A stream gets active (only users subscribed to the stream gets notified).'
    )


def delete_periodic_tasks(apps, schema_editor):
    """Delete radio notifications periodic task"""
    PeriodicTask = apps.get_model('django_celery_beat', 'PeriodicTask')  # noqa
    PeriodicTask.objects.filter(
        name='Streams notifications'
    ).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('radio', '0006_auto_20220302_1336'),
    ]

    operations = [
        migrations.RunPython(
            delete_periodic_tasks,
            create_periodic_tasks
        )
    ]
