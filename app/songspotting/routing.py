from channels.routing import URLRouter
from django.conf.urls import url

import chat.routing
import radio.routing

websocket_urlpatterns = [
    url(r'^chat/', URLRouter(chat.routing.websocket_urlpatterns)),
    url(r'^radio/', URLRouter(radio.routing.websocket_urlpatterns)),
]
