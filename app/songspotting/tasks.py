# -*- coding=utf-8 -*-
import logging

from django.core.management import call_command

from songspotting.celery import app


logger = logging.getLogger(__name__)


@app.task
def clear_sessions():
    """
    Clear expired Django sessions
    """
    call_command('clearsessions')
    logger.info('Expired sessions cleared')
