import logging
import os

from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model


logger = logging.getLogger()

User = get_user_model()


class Command(BaseCommand):
    def handle(self, *args, **options):
        if User.objects.filter(is_superuser=True).count() == 0:
            username = os.environ.get('ADMIN_USERNAME', default='admin')
            password = os.environ.get('ADMIN_PASSWORD', default='admin')
            email = os.environ.get('ADMIN_EMAIL')
            logger.info(f'Creating admin account with username "{username}"')
            User.objects.create_superuser(email=email, username=username, password=password)
        else:
            logger.info('Admin user can only be initialized if no superusers exist')
