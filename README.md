# SongSpotting

__SongSpotting__ is a Django application providing functionalities to host a WebRadio server.

It leverages __Icecast__ broadcasting technologies and adds a nicer UI and streams update notifications via email.

## Installation

The application can be run in two different ways:
 - In a docker container _(Recommended)_
 - On the host machine in a Python virtual environment

### Docker container

- Pull the docker image:
  ```
  $ docker pull registry.gitlab.com/simonepozzoli/songspotting songspotting
  ```

- Run the container:
  ```
  $ docker run -it --rm -e SECRET_KEY=my-secret-key -e EMAIL_HOST_USER=email-user -e EMAIL_HOST_PASSWORD=email-pass -e ICECAST_HOST=icecast.local -e DISCOGS_USER_TOKEN=my-discogs-token songspotting
  ```

#### Building the image

- Clone the Git repository:
  ```
  $ git clone https://gitlab.com/simonepozzoli/songspotting.git
  ```

- Build Docker image:
  ```
  $ docker build . -t songspotting
  ```

### Virtual environment

>Requires Python >= 3.8

- Clone the Git repository:
  ```
  $ git clone https://gitlab.com/simonepozzoli/songspotting.git
  ```


- Install Python requirements:
  ```
  $ cd songspotting
  $ virtualenv venv
  $ source venv/bin/activate
  $ pip install -r requirements.txt
  ```


- Collect static files:

  Update the `STATIC_ROOT` setting in `songspotting/settings.py` file: this will be the directory where static files
  will be stored, then run:
  ```
  $ python manage.py collectstatic
  ```


- Create the database:

  ```
  $ python manage.py migrate
  ```
  By default, __SongSpotting__ uses a sqlite3 database.


- Create a superuser:

  ```
  $ python manage.py createsuperuser
  ```


- Install [Icecast](https://icecast.org/).


- Install [Redis](https://redis.io/).

## Configuration

Configuration is done via environment variables:

| Name                    | Type    | Example                                               | Default                                     | Description                                                                                                                                                            |
|-------------------------|---------|-------------------------------------------------------|---------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| SECRET_KEY              | String  | )j%0!d6)c)05+mtht09qt9wtsj)#c=$u0kww&0a)9b3j%h_hn@    |                                             | Django secret key (https://docs.djangoproject.com/en/4.2/ref/settings/#secret-key)                                                                                     |
| DEBUG                   | Boolean | True \| False \| true \| false \| 1 \| 0 \| yes \| no | False                                       | Debug enabled (https://docs.djangoproject.com/en/4.2/ref/settings/#debug)                                                                                              |
| SECURE_PROXY_SSL_HEADER | String  | HTTP_X_FORWARDED_PROTO                                | ''                                          | Useful when running the application behind a reverse proxy (https://docs.djangoproject.com/en/4.2/ref/settings/#secure-proxy-ssl-header)                               |
| SECURE_COOKIES          | Boolean | True \| False \| true \| false \| 1 \| 0 \| yes \| no | True                                        | Cookies should be Secure                                                                                                                                               |
| POSTGRES_DB             | String  | songspotting \| songspotting_db                       | songspotting                                | PosgreSQL database name                                                                                                                                                |
| POSTGRES_USER           | String  | postgresuser                                          |                                             | PostgreSQL database user                                                                                                                                               |
| POSTGRES_PASSWORD       | String  | postgrespass                                          |                                             | PostgreSQL database password                                                                                                                                           |
| POSTGRES_HOST           | String  | 192.168.1.16 \| postgres.local \| postgres.myradio.fm |                                             | The hostname of the postgres server                                                                                                                                    |
| POSTGRES_PORT           | Integer | 5432                                                  | 5432                                        | The port number of the postgres server                                                                                                                                 |
| REDIS_HOST              | String  | 192.168.1.28 \| redis.local \| redis.myradio.fm       |                                             | The hostname of the redis server                                                                                                                                       |
| REDIS_PORT              | Integer | 6379                                                  | 6379                                        | The port number of the redis server                                                                                                                                    |
| ICECAST_HOST            | String  | 192.168.1.44 \| icecast.local \| icecast.myradio.fm   |                                             | The hostname of the Icecast server. SongSpotting talks to icecast only with HTTP. If you have to use HTTPS, you will need to setup a proxy.                            |
| ICECAST_PORT            | Integer | 8000                                                  | 8000                                        | The port number of the Icecast server                                                                                                                                  |
| ICECAST_SOURCE_USER     | String  | source                                                | source                                      | The Icecast source user                                                                                                                                                |
| ICECAST_SOURCE_PASSWORD | String  | hackme                                                | hackme                                      | The icecast source password (https://icecast.org/docs/icecast-2.4.0/config-file.html#:~:text=sources%20and%20relays.-,source%2Dpassword,-The%20unencrypted%20password) |
| LISTEN_TOKEN_AGE        | Integer | 3600                                                  | 3600                                        | The duration in seconds of the validity of listen tokens. This sets for how long a M3U or XSPF file is valid after download.                                           |
| EMAIL_BACKEND           | String  | django.core.mail.backends.smtp.EmailBackend           | django.core.mail.backends.smtp.EmailBackend | The email backend to use (https://docs.djangoproject.com/en/4.2/ref/settings/#email-backend)                                                                           |
| EMAIL_HOST              | String  | smtp.gmail.com                                        | smtp.gmail.com                              | The hostname of the email server (https://docs.djangoproject.com/en/4.2/ref/settings/#email-host)                                                                      |
| EMAIL_PORT              | Integer | 587                                                   | 587                                         | The port number of the email server                                                                                                                                    |
| EMAIL_USE_TLS           | Boolean | True \| False \| true \| false \| 1 \| 0 \| yes \| no | True                                        | Whether to use a TLS (secure) connection when talking to the SMTP server (https://docs.djangoproject.com/en/4.2/ref/settings/#email-use-tls)                           |
| EMAIL_HOST_USER         | String  | info.songspotting@gmail.com                           |                                             | The email server username (https://docs.djangoproject.com/en/4.2/ref/settings/#email-host-user)                                                                        |
| EMAIL_HOST_PASSWORD     | String  | my-email-password                                     |                                             | The email server password (https://docs.djangoproject.com/en/4.2/ref/settings/#email-host-password)                                                                    |
| DEFAULT_FROM_EMAIL      | String  | SongSpotting \| MyWebRadio                            | <{EMAIL_HOST_USER}>                         | The sender name displayed in emails (https://docs.djangoproject.com/en/4.2/ref/settings/#default-from-email)                                                           |
| DISCOGS_USER_TOKEN      | String  | my-discogs-token                                      |                                             | The user token for the Discogs API (https://www.discogs.com/settings/developers)                                                                                       |

See the `docker-compose.yml` file for an example configuration.

## Deploy

Nothing fancy here, deployment of __SongSpotting__ is done as any other Django application. Follow the
[documentation](https://docs.djangoproject.com/en/3.2/howto/deployment/) for some help.

If you run it in a Docker container, you can set up a reverse proxy using Apache or Nginx.
